require("dotenv").config();
const mongoose = require('mongoose'); 

const {
  MONGO_USER,
  MONGO_PWD,
  MONGO_DB,
  MONGO_URL,
} = process.env;

const options = {
  useNewUrlParser: true,
  connectTimeoutMS: 10000,
};


const initMongo = () => { 
  mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PWD}@${MONGO_URL}:27017/${MONGO_DB}`, options);
  mongoose.connection.once('open', () => { 
    console.log('Success Connected to MongoDB'); 
  }); 
  
  mongoose.connection.on('error', console.error); 
} 
module.exports = initMongo;
