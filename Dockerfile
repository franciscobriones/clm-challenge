FROM node:14-alpine

WORKDIR /usr/app

COPY package.json .

RUN npm install --quiet

ENV MONGO_USER=db_user
ENV MONGO_PWD=db_password
ENV MONGO_DB=clm_challenge

COPY . .