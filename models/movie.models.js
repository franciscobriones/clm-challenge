const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MovieSchema = new Schema({
  Title: {
    type: "String",
    unique: true,
  },
  Year: {
    type: "Date",
  },
  Released: {
    type: "Date",
  },

  Genre: {
    type: "String",
  },
  Director: {
    type: "String",
  },

  Actors: {
    type: "String",
  },
  Plot: {
    type: "String",
  },
 
  Ratings: {
    type: ["Mixed"],
  },
});
module.exports = mongoose.model("Movie", MovieSchema);
