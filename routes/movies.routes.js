const Router = require("@koa/router");

const { getAllMovies, getMovie, postMovie } = require("../services/movies.services");

//Define the prefix of the api
const router = new Router({
  prefix: "/movies",
});

//GET request
router.get("/", async (ctx) => {
  const headers = ctx.headers;
  ctx.body = await getAllMovies(headers);
});

//GET request to get a specific product
//:id is used to indicate that id is a parameter not a path
router.get("/:name", async (ctx) => {
  try {
    const name = ctx.params.name;
    const year = ctx.headers.year;
    ctx.body = await getMovie(name, year);
  }catch(err){
    ctx.status = err.statusCode || err.status || 500;
    ctx.body = {
      message: err.message
    };
  }
 
});

//GET request to get a specific product
//:id is used to indicate that id is a parameter not a path
router.post("/", async (ctx) => {
  try {
    const body = ctx.request.body;
    ctx.body = await postMovie(body);
  }catch(err){
    ctx.status = err.statusCode || err.status || 500;
    ctx.body = {
      message: err.message
    };
  }
 
});

module.exports = router;