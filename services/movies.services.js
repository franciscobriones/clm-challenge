const Movie = require("../models/movie.models");
const axios = require("axios");

const omdbApi = "http://www.omdbapi.com/?apikey=ad7fca01&";

/**
 *
 * @param {*} name required
 * @param {*} headers optionals
 * @returns a movie named
 */
const getMovie = async (name, year) => {
  /**
   * TODO:DONE
   * El valor a buscar debe ir en la URL de la API
   * Adicionalmente puede ir un header opcional que contenga el año de la película.
   * Almacenar en una BD Mongo, la siguiente info: Title Year Released Genre Director Actors Plot Ratings
   * El registro de la película solo debe estar una vez en la BD.
   * Devolver la información almacenada en la BD.
   */
  try {
    const requestUrl =
      year == undefined
        ? `${omdbApi}t=${name}`
        : `${omdbApi}t=${name}&y=${year}`;
    const { status, data } = await axios.get(requestUrl);
    if (status === 200) {
      const movie = await Movie.create(data);
      return movie;
    }
  } catch (error) {
    throw {
      statusCode: 409,
      message: error.message,
    };
  }
};

/**
 *
 * @returns all movies
 */
const getAllMovies = async (headers) => {
  const pageOptions = {
    page: parseInt(headers.page, 10) || 0,
    limit: parseInt(headers.limit, 10) || 10,
  };
  const data = await Movie.find()
    .skip(pageOptions.page * pageOptions.limit)
    .limit(pageOptions.limit);
  return {movies: data, ...pageOptions};
};

/**
 * 
 * @param {*} body
 * returns a movie string edited  
 */
const postMovie = async (body) => {
  const {movie, find, replace} = body;
  const movieReg = movie.replace(/\b\w/g, letra => letra.toUpperCase());
  const findReg = find.replace(/\b\w/g, letra => letra.toUpperCase());
  const movieFind = await Movie.findOne({Title: movieReg});
  movieFind.Plot = movieFind.Plot.replace(findReg, replace);
  return {wordChanged: movieFind.Plot, movie: movieFind};
}

module.exports = {
  getMovie,
  getAllMovies,
  postMovie
};
