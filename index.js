const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const logger = require("koa-logger");
const initMongo = require("./database");
initMongo();
const movieRoutes = require("./routes/movies.routes");

const app = new Koa();

// Middlewares
app.use(bodyParser());
app.use(logger());

app.use(movieRoutes.routes()).use(movieRoutes.allowedMethods());

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Koa is listening to http://localhost:${PORT}`);
  console.info( 'ROUTES MAP',
    movieRoutes.stack.map((i) => {
      return { PATH: i.path, METHOD: i.methods };
    })
  );
});
